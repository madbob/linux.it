jQuery(function($) {
  "use strict";

  var answers = [];
  var conf;
  var $distroTable;

  var DistroPickerI18n = {
    'unknown': '?',
    'yes': 'Sì',
    'no': 'No',
  };

  // Load the quiz configuration from the external JSON file
  $.getJSON("/js/conf.json", function(data) {
    conf = data;

    var $quizContainer = $('#quiz');

    for (var iQuestion in conf.questions) {
      var question = conf.questions[iQuestion];

      // Question container.
      var $questionContainer = $('<div>');

      // Question title.
      $('<h2>').text(question.question)
               .appendTo($questionContainer);

      // Question options.
      var $answersList = $('<ul>');
      for (var iAnswer in question.answers) {
        var answerID = 'q-' + iQuestion + '-a-' + iAnswer;
        var answer = question.answers[iAnswer];
        var $li = $('<li>');

        // Answer single option.
        var $answer = $('<input>')
          .attr('type', 'radio')
          .attr('name', iQuestion)
          .attr('id', answerID)
          .val(iAnswer);

        var $answerLabel = $('<label>')
          .attr('for', answerID)
          .text(answer);

        $li.append($answer, $answerLabel);
        $answersList.append($li);
      }

      $questionContainer.append($answersList);
      $quizContainer.append($questionContainer);

      answers[iQuestion] = -1;
    }

    $distroTable = $('<table>')
      .attr('id', 'distros');

    var $distros = $('<tbody>')
      .appendTo($distroTable);

    for (var iDistro in conf.distributions) {
      var distro = conf.distributions[iDistro];
      var $row = $('<tr>');

      $row.append(
        $('<td>').append(
          $('<img>').attr('src', distro.logo)));

      $row.append(
          $('<td>').text(distro.name));

      for (var i in conf.questions) {
        $row.append(
          $('<td>').addClass('answer')
                   .text(DistroPickerI18n.unknown));
      }

      $row.append(
        $('<td>')
          .addClass('answer')
          .append(
            $('<a>').attr('target', '_blank')
                    .attr('rel', 'nofollow')
                    .attr('href', distro.website)
                    .text('Scarica')));

      $distros.append($row);
    }

    $quizContainer.append($distroTable);

    $('#quiz').on('change', 'input[type=radio]', function() {
      var selected  = parseInt($(this).val());
      var iQuestion = parseInt($(this).attr('name'));

      answers[iQuestion] = selected;

      // Update each column for each distribution.
      for (var d in conf.distributions) {
        var distro = conf.distributions[d];
        var isSelected = distro.answers[iQuestion] === selected;

        // Update Yes/No cell.
        var $cell = $distroTable.find('tr').eq(d).find('td').eq(iQuestion + 2);
        $cell.toggleClass( 'positive',  isSelected )
             .toggleClass( 'negative', !isSelected )
             .text( isSelected ? DistroPickerI18n.yes : DistroPickerI18n.no );
      }

      var complete = true;
      for (var i in answers) {
        if (i === -1) {
          complete = false;
          break;
        }
      }
    });
  });
});
